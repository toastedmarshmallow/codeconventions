﻿/* ..\CodeConventions\Program.cs */
/*-------------------------------------------*/
/*Index*/

/*1. Global convention for comments*/
/*2. Global Variables*/
/*3. Methods and functions conventions*/
/*4. Variables within a function*/
/*5. String Interpolation*/
/*6. Class instances*/
/*7. Class declarations*/
/*8. Function Declarations*/
/*9. Variable containing units*/
       
    
/*NB*/
/*Toasted Marshmallow makes use of Tabs as opposed to spaces*/
/*The top line of a file should indicate the file url relative to the root folder*/

using System;

namespace CodeConventions
{
    class Program
    {
        /*Comments that indicate index or purpose of code should be completed via asterix (/*) notation*/
            //Comments for deliberation should be indented and completed via double slash (//) notation

        /*-------------------------------------------*/ 
        /*-----Using hyphens to separate code blocks and indices (revise)-----*/ 


        /*2. Global Variables*/
         //global variables are named in ALL_CAPS with underscores
        public enum MARSHMALLOW_SIZES
        {
            small, medium, large, superSized
        };

        static void Main(string[] args)
        {
        /*3. Methods and functions conventions*/
            //comments inside methods/functions are all lowercase unless using words like IP-address,etc.

        /*4. Variables within a function*/
            //variables within a function are camel case starting with a lower letter 
            var someVariable = "text text text";
        
        /*5. String Interpolation*/
            //string interpolation
            string name = "Marshall";
            Console.WriteLine($"Hello {name}!");
        
        /*6. Class instances*/
            //creating an instance of a class - use variable naming conventions
            Marshmallow marshmallowNumberOne = new Marshmallow(MARSHMALLOW_SIZES.medium);
            marshmallowNumberOne.toastMarshmallow(22000);

        }
        /*7. Class declarations*/
            //classes are named using camel-case starting with a capital letter
            //classes must always include a basic description
            /// <summary>
            /// The Marshmallow class 
            /// </summary>
        public class Marshmallow
        {

            public bool IsToasted { get; private set; }
            public bool IsBurnt { get; private set; }
            public MARSHMALLOW_SIZES Size { get; private set; }


            public Marshmallow(MARSHMALLOW_SIZES size)
            {
                Size = size;
            }
        /*8. Function Declarations*/
            //functions must always include a basic description
            //functions are named using camel case with a lower character first
            /// <summary>
            /// This function sets the IsToasted property to true.
            /// </summary>
            public void toastMarshmallow()
            {
                IsToasted = true;
            }

            /// <summary>
            /// This function will toast the marshmallow for a set time.
            /// </summary>
            /// <param name="forTime_ms"></param>
            public void toastMarshmallow(int forTime_ms)
            {
                /// <param name="forTime_ms">
                /// This parameter is the time that the marshmallow is toasted in milliseconds.
                /// </param>
        /*9. Variable containing units*/
                //if a variable includes a unit then it must be denoted by an underscore
                //(i.e. forTime_sec, forTime_ms, forDistance_km, etc.)
                if (forTime_ms > 20000)
                    IsToasted = true;
                else if (forTime_ms > 40000)
                {
                    IsToasted = false;
                    IsBurnt = true;
                }
            }
        }
    }
}
